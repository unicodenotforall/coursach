#include "UserManager.h"
#include "User.h"
#include "UserFactory.h"
#include "crypto.h"
#include "console.h"
#include "exceptions.h"
#include <fstream>
#include <iostream>

UserManager::UserManager()
{
}

vector<User> UserManager::loadUsersFromFile()
{
	fstream input;
	Course::user_db userdb;

	vector<User> users;

	input.open("users.dat", fstream::binary | fstream::in);
	if (!input.is_open()) {
		cout << "Can't open file users.dat" << endl;
		cout << "Create a new one? [Y/N]" << endl;
		char i;
		User adm;
		while (cin >> i) {
			switch (tolower(i)) {
			case 'y':
				UserFactory userFactory;
				adm = userFactory.makeDefaultAdmin();
				users.push_back(adm);
				this->saveUsersToFile(&users);
				return users;
			case 'n':
				exit(EXIT_FAILURE);
			default:
				cout << "Please enter Y or N" << endl;
				break;
			}
		}


		exit(0);
	}
	userdb.ParseFromIstream(&input);
	UserFactory userFactory;
	for (int i = 0; i < userdb.users_size(); i++) {
		const auto u = userdb.users(i);
		auto user = userFactory.fromProtobuf(&u);
		users.push_back(user);
	}

	return users;
}

User UserManager::addNewUser()
{
	string username;
	string password;
	string passwordc;
	Role role;

	cout << "Enter desired username: ";

	while (cin >> username) {
		cout << endl;

		auto users = this->loadUsersFromFile();
		try {
			auto u = this->findByUsername(username);
			cout << "User with same name already exists. Chose another name." << endl;
			cout << "Enter desired username: ";

		}
		catch (UserNotFoundException &e) {
			break;
		}
	}

	Console::setStdinEcho(false);
	cout << "Enter password: ";
	while (cin >> password) {
		cout << endl;
		cout << "Repeat password: ";
		cin >> passwordc;
		cout << endl;

		if (password.compare(passwordc)) {
			cout << "Passwords mismatch. Try again" << endl;
			cout << "Enter password: ";
		}
		else
			break;
	}
	Console::setStdinEcho(true);

	cout << "Administrator? [Y/N]" << endl;

	char i;
	bool inrole = true;
	while (inrole) {
		cin >> i;
		switch (tolower(i)) {
		case 'y':
			role = Role::R_ADMIN;
			inrole = false;
			break;
		case 'n':
			role = Role::R_USER;
			inrole = false;
			break;
		default:
			cout << "Please enter Y or N" << endl;
			break;
		}


	}

	User newUser(username, Crypto::crypt(password), role);
	try {
		writeUser(newUser);
	}
	catch (exception &e) {
		cout << "Could not add a new user. " << e.what() << endl;
	}
	cout << "User added successfully." << endl;
	return newUser;
}

User UserManager::findByUsername(string username) {
	auto users = this->loadUsersFromFile();
	auto it = find_if(users.begin(), users.end(), [&username](User& usr) {return usr.getUsername() == username; });
	if (it == users.end())
		throw UserNotFoundException();

	return *it;
}

void UserManager::deleteUser(string username)
{
	auto users = this->loadUsersFromFile();
	auto it = find_if(users.begin(), users.end(), [&username](User& usr) {return usr.getUsername() == username; });
	if (it == users.end()) {
		cout << "Could not found the requested user" << endl;
	}
	else {
		users.erase(it);
		this->saveUsersToFile(&users);
		cout << "User: " << username << " deleted" << endl;
	}

}

User UserManager::updateUser(User user, User newUser)
{
	User u;
	try {
		u = findByUsername(user.getUsername());
		auto users = loadUsersFromFile();
		auto it = find_if(users.begin(), users.end(), [&user](User& usr) {return usr.getUsername() == user.getUsername(); });
		*it = newUser;
		saveUsersToFile(&users);

	}
	catch (UserNotFoundException &e) {
		cout << "Could not find specified user" << endl;

	}
	catch (runtime_error &e) {
		cout << "An unexpexted error occured: " << e.what() << endl;
	}
	return User();
}



void UserManager::saveUsersToFile(vector<User> *users)
{
	Course::user_db usrdb;
	fstream fs;

	fs.open("users.dat", fstream::binary | fstream::out);

	for (auto u : *users) {
		auto userdata = usrdb.add_users();
		*userdata = u.toProtobuf();
	}

	string output = usrdb.SerializePartialAsString();
	fs << output;
	fs.close();
}

void UserManager::writeUser(User user)
{
	auto users = this->loadUsersFromFile();
	users.push_back(user);
	saveUsersToFile(&users);
}


UserManager::~UserManager()
{
}
