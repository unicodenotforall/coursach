#include "MenuItem.h"

MenuItem::MenuItem(string label, void(*callback)()) {
	this->label = label;
	this->action_callback = callback;
}

string MenuItem::getLabel() {
	return label;
}