#include "Menu.h"
#include "console.h"
#include <iostream>

using namespace std;

Menu::Menu(string header) {
	this->header = header;
}

void Menu::addItem(MenuItem item) {
	this->menuItems.push_back(item);
}

const vector<MenuItem>& Menu::getItems() const {
	return menuItems;
}

void Menu::show() {
	cout << endl;
	cout << header << endl;

	for (unsigned int i = 0; i < menuItems.size(); i++) {
		cout << i + 1 << " - " << menuItems[i].getLabel() << endl;
	}
}

void Menu::process() {
	unsigned int n;
	string input;
	while (true) {
		while (cin >> input) {
			cout << "Enter a action number: " << endl;

			n = stol(input);
			if (!n)
				cout << "You should pass an integer" << endl;
			else break;

			try {
				n = stol(input);
				break;
			}
			catch (invalid_argument &e) {
				cout << "You should pass an integer" << endl;
			}
		}
		if (n) {
			if (n > this->menuItems.size()) {
				cout << "No such menu item. Exiting from menu . . ." << endl;
				break;
			}
			Console::clearScreen();
			this->menuItems[n - 1].action_callback();
			cout << "Press Enter to Continue" << endl;
			cin.ignore(256, '\n');
			cin.sync();
			cin.get();
			Console::clearScreen();
			this->show();
		}
	}
}