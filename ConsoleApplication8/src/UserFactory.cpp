#include "UserFactory.h"
#include "crypto.h"

User UserFactory::fromProtobuf(const Course::user_data *usrd) {
	return User(usrd->username(), usrd->password(), (Role)usrd->role());
}

User UserFactory::makeDefaultAdmin() {
	return User("Blitzkrieg", Crypto::crypt("322"), Role::R_ADMIN);
}