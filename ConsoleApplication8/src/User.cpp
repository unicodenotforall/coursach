#include "User.h"

User::User() {}
User::User(string username, string password, Role role) {
	this->username = username;
	this->password = password;
	this->role = role;
}

string User::getUsername() {
	return this->username;
}

string User::getPassword() {
	return this->password;
}

void User::setUsername(string name)
{
	this->username = name;
}

void User::setPassword(string password)
{
	this->password = password;
}

Role User::getRole() {
	return this->role;
}

Course::user_data User::toProtobuf() {
	Course::user_data ud;
	ud.set_username(username);
	ud.set_password(password);
	ud.set_role(role);

	return ud;
}