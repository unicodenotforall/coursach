#ifdef _WIN32
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <iomanip>
#include <limits>

#include "user_data.pb.h"
#include "User.h"
#include "Menu.h"
#include "MenuItem.h"
#include "Role.h"
#include "UserFactory.h"
#include "auth.h"

#define ESC "\x1b"
#define CSI "\x1b["

using namespace std;

// ����������� ����

void add_user() {
	UserManager userManager;
	userManager.addNewUser();
}

void menu_action2() {
	UserManager userManager;
	cout << endl;
	auto users = userManager.loadUsersFromFile();
	cout << "|" << setw(12) << "User Name" << setw(12) << "|" << setw(12) << "Role" << setw(12) << "|" << endl;
	for (auto u : users) {
		cout << "|" << setw(12) << u.getUsername() << setw(12) << "|" << setw(12) << (u.getRole() == 0 ? "Admin" : "User") << setw(12) << "|" << endl;
	}

}

void menu_action3() {
	string username;
	cout << "Remove user" << endl;
	cout << "username: ";
	cin >> username;
	UserManager userManager;
	userManager.deleteUser(username);
}

void menu_action4() {
	UserManager userManager;
	string username;
	User user;
	User newUser;
	cout << "username: ";
	cin >> username;
	try {
		user = userManager.findByUsername(username);
		newUser = user;

		cout << "Change username? [Y/N]" << endl;

		char i;
		bool p = true;
		while (p) {
			cin >> i;
			switch (tolower(i)) {
			case 'y':
				cout << "New username: ";
				cin >> username;
				newUser.setUsername(username);
				p = false;
				break;
			case 'n':
				p = false;
				break;
			default:
				cout << "Please enter Y or N" << endl;
				break;
			}


		}

		cout << "Change password? [Y/N]" << endl;
		p = true;
		string password;
		string passwordc;
		while (p) {
			cin >> i;
			switch (tolower(i)) {
			case 'y':
				cout << "New password: ";
				while (cin >> password) {
					cout << endl;
					cout << "Repeat password: ";
					cin >> passwordc;
					cout << endl;

					if (password.compare(passwordc)) {
						cout << "Passwords mismatch. Try again" << endl;
						cout << "New password: ";
					}
					else
						break;
				}
				newUser.setPassword(Crypto::crypt(password));
				p = false;
				break;
			case 'n':
				p = false;
				break;
			default:
				cout << "Please enter Y or N" << endl;
				break;
			}


		}

		userManager.updateUser(user, newUser);

	}
	catch (UserNotFoundException &e) {
		cout << "Couldn't find specified user" << endl;
	}


}

void menu_action5() {
	cout << "Are you sure? [Y/N]" << endl;
	char i;
	bool c = true;
	while (c) {
		cin >> i;
		switch (tolower(i)) {
		case 'y':
			exit(0);
		case 'n':
			return;
		default:
			cout << "Please enter Y or N" << endl;
			break;
		}
	}
}


int main(int argc, char *argv[])
{
	GOOGLE_PROTOBUF_VERIFY_VERSION;
	Console::enableVT();

	auto login_result = login();

	// ������� ����

	Menu adminMenu("------------- Admin Menu -------------");

	adminMenu.addItem(MenuItem("Add new user", &add_user));
	adminMenu.addItem(MenuItem("List users", &menu_action2));
	adminMenu.addItem(MenuItem("Remove user", &menu_action3));
	adminMenu.addItem(MenuItem("Perform Action 4", &menu_action4));
	adminMenu.addItem(MenuItem("Exit", &menu_action5));

	if (login_result.getRole() == Role::R_ADMIN) {
		Console::clearScreen();
		adminMenu.show();
		adminMenu.process();
	}

	return 0;

}