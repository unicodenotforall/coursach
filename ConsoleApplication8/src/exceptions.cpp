#include "exceptions.h"

const char *UserNotFoundException::what()
{
	return (const char*) "User with such name could not be found";
}
