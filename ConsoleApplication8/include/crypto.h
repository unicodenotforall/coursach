#pragma once
#include <string>
#include <openssl/sha.h>
#include <openssl/rand.h>

using namespace std;

class Crypto {
public:
	static string crypt(string password) {
		SHA256_CTX context;
		unsigned char md1[SHA256_DIGEST_LENGTH];
		char *mdString = new char[SHA256_DIGEST_LENGTH * 2 + 1];

		SHA256_Init(&context);
		SHA256_Update(&context, (unsigned char*)password.c_str(), strlen(password.c_str()));
		SHA256_Final(md1, &context);

		for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
			sprintf(&mdString[i * 2], "%02x", (unsigned int)md1[i]);

		string hash(mdString);

		delete[] mdString;

		return hash;
	}
};
