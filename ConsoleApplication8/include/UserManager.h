#pragma once
#include "User.h"
#include <vector>
using namespace std;
class UserManager
{
public:
	UserManager();
	vector<User> loadUsersFromFile();
	User addNewUser();
	void deleteUser(string username);
	User updateUser(User user, User newUser);
	void saveUsersToFile(vector<User> *users);
	void writeUser(User user);
	User findByUsername(string username);
	~UserManager();
};

