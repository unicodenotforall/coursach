#pragma once

#ifdef _WIN32
#include <windows.h>
#include <VersionHelpers.h>
#include <consoleapi.h>
// Standard error macro for reporting API errors
#define PERR(bSuccess, api){if(!(bSuccess)) printf("%s:Error %d from %s \
on line %d\n", __FILE__, GetLastError(), api, __LINE__);}
#else
#include <termios.h>
#include <unistd.h>
#endif

#include <iostream>

using namespace std;

class Console {
public:
	static void setStdinEcho(bool enable = true)
	{
#ifdef _WIN32
		HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);
		DWORD mode;
		GetConsoleMode(hStdin, &mode);

		if (!enable)
			mode &= ~ENABLE_ECHO_INPUT;
		else
			mode |= ENABLE_ECHO_INPUT;

		SetConsoleMode(hStdin, mode);

#else
		struct termios tty;
		tcgetattr(STDIN_FILENO, &tty);
		if (!enable)
			tty.c_lflag &= ~ECHO;
		else
			tty.c_lflag |= ECHO;

		(void)tcsetattr(STDIN_FILENO, TCSANOW, &tty);
#endif
	}

	static void clearScreen() {
#ifdef _WIN32
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		COORD coordScreen = { 0, 0 };    /* here's where we'll home the
										 cursor */
		BOOL bSuccess;
		DWORD cCharsWritten;
		CONSOLE_SCREEN_BUFFER_INFO csbi; /* to get buffer info */
		DWORD dwConSize;                 /* number of character cells in
										 the current buffer */

										 /* get the number of character cells in the current buffer */

		bSuccess = GetConsoleScreenBufferInfo(hConsole, &csbi);
		PERR(bSuccess, "GetConsoleScreenBufferInfo");
		dwConSize = csbi.dwSize.X * csbi.dwSize.Y;

		/* fill the entire screen with blanks */

		bSuccess = FillConsoleOutputCharacter(hConsole, (TCHAR) ' ',
			dwConSize, coordScreen, &cCharsWritten);
		PERR(bSuccess, "FillConsoleOutputCharacter");

		/* get the current text attribute */

		bSuccess = GetConsoleScreenBufferInfo(hConsole, &csbi);
		PERR(bSuccess, "ConsoleScreenBufferInfo");

		/* now set the buffer's attributes accordingly */

		bSuccess = FillConsoleOutputAttribute(hConsole, csbi.wAttributes,
			dwConSize, coordScreen, &cCharsWritten);
		PERR(bSuccess, "FillConsoleOutputAttribute");

		/* put the cursor at (0, 0) */

		bSuccess = SetConsoleCursorPosition(hConsole, coordScreen);
		PERR(bSuccess, "SetConsoleCursorPosition");
		return;
#else
		cout << "\e[1;1H\e[2J";
#endif
	}

#ifdef _WIN32
	static int enableVT() {
		if (!IsWindowsVersionOrGreater(6, 0, 0)) {
			cout << "Full VT emulation is available only on Windows 10 Anniversary Edition" << endl;
			return -1;
		}


#ifdef ENABLE_VIRTUAL_TERMINAL_PROCESSING
		HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
		if (hOut == INVALID_HANDLE_VALUE)
		{
			return false;
		}
		HANDLE hIn = GetStdHandle(STD_INPUT_HANDLE);
		if (hIn == INVALID_HANDLE_VALUE)
		{
			return false;
		}

		DWORD dwOriginalOutMode = 0;
		DWORD dwOriginalInMode = 0;
		if (!GetConsoleMode(hOut, &dwOriginalOutMode))
		{
			return false;
		}
		if (!GetConsoleMode(hIn, &dwOriginalInMode))
		{
			return false;
		}

		DWORD dwRequestedOutModes = ENABLE_VIRTUAL_TERMINAL_PROCESSING | DISABLE_NEWLINE_AUTO_RETURN;
		DWORD dwRequestedInModes = ENABLE_VIRTUAL_TERMINAL_INPUT;

		DWORD dwOutMode = dwOriginalOutMode | dwRequestedOutModes;
		if (!SetConsoleMode(hOut, dwOutMode))
		{
			// we failed to set both modes, try to step down mode gracefully.
			dwRequestedOutModes = ENABLE_VIRTUAL_TERMINAL_PROCESSING;
			dwOutMode = dwOriginalOutMode | dwRequestedOutModes;
			if (!SetConsoleMode(hOut, dwOutMode))
			{
				// Failed to set any VT mode, can't do anything here.
				return -1;
			}
		}

		DWORD dwInMode = dwOriginalInMode | ENABLE_VIRTUAL_TERMINAL_INPUT;
		if (!SetConsoleMode(hIn, dwInMode))
		{
			// Failed to set VT input mode, can't do anything here.
			return -1;
		}
#endif

		return 0;
	}
#endif
};

