#pragma once
#include <exception>

using namespace std;

class UserNotFoundException : exception {
public:
	virtual const char *what();
};