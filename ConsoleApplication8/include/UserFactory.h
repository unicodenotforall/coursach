#pragma once
#include "User.h"
#include "user_data.pb.h"
class UserFactory {
public:
	User fromProtobuf(const Course::user_data *usrd);
	User makeDefaultAdmin();
};