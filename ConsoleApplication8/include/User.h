#pragma once

#include <string>

#include "user_data.pb.h"
#include "Role.h"

using namespace std;

class User {
public:
	User();
	User(string username, string password, Role role);
	string getUsername();
	string getPassword();
	void setUsername(string name);
	void setPassword(string password);
	Role getRole();
	Course::user_data toProtobuf();

protected:
	string username;
	string password;
	Role role;
};