#pragma once
#include <string>
using namespace std;
class MenuItem {
public:
	MenuItem(string label, void(*callback)());
	void(*action_callback)();
	string getLabel();

protected:
	string label;
};