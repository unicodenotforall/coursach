#pragma once
#include "User.h"
#include "console.h"
#include "crypto.h"
#include "UserManager.h"
#include <string>
#include <iostream>
#include "exceptions.h"

using namespace std;

User login() {
	string username;
	string password;
	User user;
	UserManager userManager;
	int tries = 0;

	cout << "Welcome to MySuperProg1337 v 1.0" << endl;

	cout << "login: ";
	while (cin >> username) {

		if (tries > 2) {
			cout << "Maximum number of tries reached. Exiting." << endl;
			exit(EXIT_FAILURE);
		}

		try {
			user = userManager.findByUsername(username);
			break;
		}
		catch (UserNotFoundException &e) {
			cout << "User with such name not found. Try again." << endl;
			cout << "login: ";
			tries++;
		}
	}


	Console::setStdinEcho(false);
	cout << "password: ";
	while (cin >> password) {
		if (tries > 2) {
			cout << "Maximum number of tries reached. Exiting." << endl;
			exit(EXIT_FAILURE);
		}

		string pass = Crypto::crypt(password);
		if (pass.compare(user.getPassword())) {
			cout << "Try again" << endl;
			cout << "password: ";
			tries++;
		}
		else {
			break;
		}
	}
	Console::setStdinEcho(true);


	return user;
}