#pragma once
#include "MenuItem.h"
#include <string>
#include <vector>


using namespace std;
class Menu {
public:
	Menu(string header);
	void addItem(MenuItem item);
	const vector<MenuItem>& getItems() const;
	void show();
	void process();

protected:
	string header;
	vector<MenuItem> menuItems;
};